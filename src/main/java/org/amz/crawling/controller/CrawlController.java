package org.amz.crawling.controller;

import java.time.LocalDateTime;

import org.amz.crawling.dto.RequestDto;
import org.amz.crawling.model.Product;
import org.amz.crawling.repository.ProductRepository;
import org.amz.crawling.util.CrawlUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CrawlController {

    private ProductRepository repository;
    public CrawlController(ProductRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public ModelAndView showPage(RequestDto requestDto) {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("requestDto", requestDto);
        modelAndView.addObject("display", false);
        return modelAndView;
    }

    @PostMapping("/")
    public ModelAndView importData(RequestDto requestDTO) throws InterruptedException {
    	String ip = requestDTO.getIp().trim();
    	String port = requestDTO.getPort().trim();
    	String userName = requestDTO.getUserName().trim();
    	String password = requestDTO.getPassword().trim();
    	
    	long start = System.currentTimeMillis();
        ModelAndView modelAndView = new ModelAndView();
        Product product = repository.findFirstByAsin(requestDTO.getAsin());
        if (product == null) {
            product = new Product();
            WebDriver driver;
            if(ip.length() == 0) {
//            	String proxy = ProxyUtils.getRandomProxy() + ":3128";
//            	ChromeOptions options = new ChromeOptions().addArguments("--proxy-server=http://" + proxy);
//            	driver = new ChromeDriver(options);
            	Proxy proxy = new Proxy();
            	proxy.setSocksUsername("vodkaexpans.custom1");
            	proxy.setSocksPassword("b36f65bc26");
            	proxy.setHttpProxy("66.42.95.53:8080");
                ChromeOptions options = new ChromeOptions(); 
                options.setCapability("proxy", proxy);
                driver = new ChromeDriver(options);
            } else {
            	Proxy proxy = new Proxy();
                if(userName.length() > 0) {
                	proxy.setSocksUsername(userName);
                }
                if (password.length() > 0) {
                	proxy.setSocksPassword(password);
                }
                proxy.setHttpProxy(ip + ":" + port);
                ChromeOptions options = new ChromeOptions(); 
                options.setCapability("proxy", proxy);
                driver = new ChromeDriver(options);
            }
            
            driver.get("https://www.amazon.com/dp/" + requestDTO.getAsin());
            String asin = requestDTO.getAsin().trim();
            product.setAsin(asin);
            modelAndView.addObject("asin", asin);
            String title = CrawlUtils.getTitle(driver);
            if (title.length() == 0) {
                modelAndView.addObject("message", "Product is not exist");
            } else {
            	String html = CrawlUtils.getHtml(driver);
            	product.setHtml(html);
                product.setTitle(title);
                modelAndView.addObject("title", title);
                String cat = CrawlUtils.getCategory(driver);
                product.setCat(cat);
                modelAndView.addObject("cat", cat);
                String depth = CrawlUtils.getDepth(driver);
                product.setDepth(depth);
                modelAndView.addObject("depth", depth);
                String fullDescription = CrawlUtils.getFullDescription(driver);
                product.setFullDescription(fullDescription);
                modelAndView.addObject("fullDescription", fullDescription);
                String height = CrawlUtils.getHeight(driver);
                product.setHeight(height);
                modelAndView.addObject("height", height);
                String images = CrawlUtils.getImages(driver);
                product.setImages(images);
                modelAndView.addObject("images", images);
                String price = CrawlUtils.getPrice(driver);
                product.setPrice(price);
                modelAndView.addObject("price", price);
                String prime = CrawlUtils.getPrime(driver);
                product.setPrime(prime);
                modelAndView.addObject("prime", prime);
                String review = CrawlUtils.getReview(driver);
                product.setReview(review);
                modelAndView.addObject("review", review);
                String shortDescription = CrawlUtils.getShortDescription(driver);
                product.setShortDescription(shortDescription);
                modelAndView.addObject("shortDescription", shortDescription);
                String star = CrawlUtils.getStar(driver);
                product.setStar(star);
                modelAndView.addObject("star", star);
                String weight = CrawlUtils.getWeight(driver);
                product.setWeight(weight);
                modelAndView.addObject("weight", weight);
                String width = CrawlUtils.getWidth(driver);
                product.setWidth(width);
                modelAndView.addObject("width", width);
                String status = CrawlUtils.getStatus(driver);
                product.setStatus(status);
                modelAndView.addObject("status", status);
                modelAndView.addObject("display", true);
                product.setUpdateTime(LocalDateTime.now());
                repository.save(product);
            }
            driver.close();
        }
        modelAndView.setViewName("index");
        long finish = System.currentTimeMillis();
        System.out.println("Processed Time: " + (finish - start)/1000);
        return modelAndView;
    }
}
