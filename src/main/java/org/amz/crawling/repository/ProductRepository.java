package org.amz.crawling.repository;

import org.amz.crawling.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository  extends JpaRepository<Product, Long> {
    Product findFirstByAsin(String asin);
}
