package org.amz.crawling.schedule;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import org.amz.crawling.model.Product;
import org.amz.crawling.repository.ProductRepository;
import org.amz.crawling.util.CrawlUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ExecutionPlan {
	
    private ProductRepository repository;
    public ExecutionPlan(ProductRepository repository) {
        this.repository = repository;
    }
    @Scheduled(fixedRate = 120*60*1000) //milisecond
    public void updateInformation() throws Exception{
    	System.out.println("Start: --- "  + LocalTime.now());
    	List<Product> products = repository.findAll();
        for(Product product: products) {
        	String currentTile = product.getTitle();
        	String currentPrice = product.getPrice();
        	String currentStatus = product.getStatus();
        	
//        	String proxy = ProxyUtils.getRandomProxy() + ":3128";
//        	ChromeOptions options = new ChromeOptions().addArguments("--proxy-server=http://" + proxy);
//            WebDriver driver = new ChromeDriver(options);
        	Proxy proxy = new Proxy();
        	proxy.setSocksUsername("vodkaexpans.custom1");
        	proxy.setSocksPassword("b36f65bc26");
        	proxy.setHttpProxy("66.42.95.53:8080");
            ChromeOptions options = new ChromeOptions(); 
            options.setCapability("proxy", proxy);
            WebDriver driver = new ChromeDriver(options);
            driver.get("https://www.amazon.com/dp/" + product.getAsin());
            
            String newTitle = CrawlUtils.getTitle(driver);
            String newPrice = CrawlUtils.getPrice(driver);
            String newStatus = CrawlUtils.getStatus(driver);
            
            if(!newTitle.equalsIgnoreCase(currentTile) || !newPrice.equalsIgnoreCase(currentPrice) || !newStatus.equalsIgnoreCase(currentStatus)) {
            	String html = CrawlUtils.getHtml(driver);
            	product.setHtml(html);
                product.setTitle(newTitle);
                String cat = CrawlUtils.getCategory(driver);
                product.setCat(cat);
                String depth = CrawlUtils.getDepth(driver);
                product.setDepth(depth);
                String fullDescription = CrawlUtils.getFullDescription(driver);
                product.setFullDescription(fullDescription);
                String height = CrawlUtils.getHeight(driver);
                product.setHeight(height);
                String images = CrawlUtils.getImages(driver);
                product.setImages(images);
                String price = CrawlUtils.getPrice(driver);
                product.setPrice(price);
                String prime = CrawlUtils.getPrime(driver);
                product.setPrime(prime);
                String review = CrawlUtils.getReview(driver);
                product.setReview(review);
                String shortDescription = CrawlUtils.getShortDescription(driver);
                product.setShortDescription(shortDescription);
                String star = CrawlUtils.getStar(driver);
                product.setStar(star);
                String weight = CrawlUtils.getWeight(driver);
                product.setWeight(weight);
                String width = CrawlUtils.getWidth(driver);
                product.setWidth(width);
                String status = CrawlUtils.getStatus(driver);
                product.setStatus(status);
                product.setUpdateTime(LocalDateTime.now());
                repository.save(product);
            }
            driver.close();
        }

    }
}
