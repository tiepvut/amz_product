package org.amz.crawling.util;


import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.exec.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CrawlUtils {
	public static String getHtml(WebDriver driver) {
		return driver.getPageSource();
	}
	
    public static String getCategory(WebDriver driver) {
        String category = "";
        try {
            WebElement divContainer = driver.findElement(By.id("wayfinding-breadcrumbs_feature_div"));
            List<WebElement> spanElements = divContainer.findElements(By.className("a-list-item"));
            List<String> categories = spanElements.stream().map(e -> e.getText()).collect(Collectors.toList());
            category = StringUtils.toString(categories.toArray(new String[0]), " ");
            return category;
        } catch (Exception ex) {
            return "";
        }
    }

    //todo: check where to get this field
    public static String getDepth(WebDriver driver) {
        String depth = "";
        String[] dimensions = getDimension(driver);
        if (dimensions.length > 0) {
            depth = getDimension(driver)[0] + "inches";
        }
        return depth;
    }

    public static String getFullDescription(WebDriver driver) {
    	String fullDescription = "";
    	String fromManuFacture = "";
    	String description = "";
    	try {
    		fromManuFacture = getFromManuFacture(driver);
    	} catch (Exception ex) {
    		fromManuFacture = "";
    	}
    	WebElement descriptionElement;
        try {
        	descriptionElement = driver.findElement(By.id("productDescription_feature_div"));
        	description = descriptionElement.getAttribute("outerHTML");
    		if (description.contains("table")) {
            	List<WebElement> tables = descriptionElement.findElements(By.tagName("table"));
            	for(WebElement table: tables) {
                    String htmlTableSource = table.getAttribute("outerHTML");
                    if(htmlTableSource.contains("/dp/")) {
                    	description = description.replace(htmlTableSource, "");
                    }
                }
            }
    		description = description.replace("data-src", "data-tmp");
    		description = description.replace("src", "modified");
    		description = description.replace("data-tmp", "src");
            fullDescription =  fromManuFacture + "<br>" + description;
        } catch (Exception ex) {
        	fullDescription = "";
        }
        if (fullDescription == "") {
        	try {
        		descriptionElement = driver.findElement(By.id("aplus"));
        		description = descriptionElement.getAttribute("outerHTML");
        		if (description.contains("table")) {
                	List<WebElement> tables = descriptionElement.findElements(By.tagName("table"));
                	for(WebElement table: tables) {
                        String htmlTableSource = table.getAttribute("outerHTML");
                        if(htmlTableSource.contains("/dp/")) {
                        	description = description.replace(htmlTableSource, "");
                        }
                    }
                }
        		description = description.replace("data-src", "data-tmp");
        		description = description.replace("src", "modified");
        		description = description.replace("data-tmp", "src");
        		fullDescription = fromManuFacture + "<br>" + description;
        	} catch (Exception ex) {
        		fullDescription = "";
        	}
        }
        return fullDescription;
    }

    private static String getFromManuFacture(WebDriver driver) {
        try {
            WebElement root = driver.findElement(By.id("aplus_feature_div"));
            String htmlSource = root.getAttribute("outerHTML");
            if (htmlSource.contains("table")) {
                List<WebElement> tables = root.findElements(By.tagName("table"));
                for(WebElement table: tables) {
                    String htmlTableSource = table.getAttribute("outerHTML");
                    if(htmlTableSource.contains("/dp/")) {
                        htmlSource = htmlSource.replace(htmlTableSource, "");
                    }
                }
            }
            htmlSource = htmlSource.replace("data-src", "data-tmp");
            htmlSource = htmlSource.replace("src", "modified");
            htmlSource = htmlSource.replace("data-tmp", "src");
            return htmlSource;
        } catch (Exception ex) {
            return "";
        }
    }
    

    //todo: check where to get this field
    public static String getHeight(WebDriver driver) {
        String height = "";
        String[] dimensions = getDimension(driver);
        if (dimensions.length > 0) {
            height = getDimension(driver)[2] + "inches";
        }
        return height;
    }

    public static String getImages(WebDriver driver) {
        try {
            List<WebElement> imageThumbnails = driver.findElements(By.className("imageThumbnail"));
            List<String> imgSrc = new LinkedList<>();
            for (WebElement imageThumbnail: imageThumbnails) {
                Actions action = new Actions(driver);
                action.moveToElement(imageThumbnail).click().build().perform();
            }
            List<WebElement> imgTagWrappers = driver.findElements(By.className("imgTagWrapper"));
            for(WebElement imgTagWrapper: imgTagWrappers) {
                String htmlFormat = imgTagWrapper.getAttribute("innerHTML");
                if(htmlFormat.contains("img")) {
                    imgSrc.add(imgTagWrapper.findElement(By.tagName("img")).getAttribute("src"));
                }
            }
            return StringUtils.toString(imgSrc.toArray(new String[0]), ",");
        } catch (Exception ex) {
            return "";
        }
    }

    public static String getPrice(WebDriver driver) {
        String price = "";
        try {
            price = driver.findElement(By.id("priceblock_ourprice")).getAttribute("innerHTML");
        } catch (Exception ex) {
        	price = "";
        }
        if (price == "") {
        	try {
        		price = driver.findElement(By.id("priceblock_saleprice")).getAttribute("innerHTML");
        	} catch (Exception ex) {
        		price = "";
        	}
        }
        if (price == "") {
        	try {
        		price = driver.findElement(By.id("priceblock_dealprice")).getAttribute("innerHTML");
        	} catch (Exception ex) {
        		price = "";
        	}
        }
        return price;
    }

    //todo: check where to get this field
    public static String getPrime(WebDriver driver) {
        return "";
    }

    public static String getReview(WebDriver driver) {
    	try {
    		return driver.findElement(By.className("card-padding")).getAttribute("outerHTML");
    	} catch (Exception ex) {
    		return "";
    	}
    }

    public static String getShortDescription(WebDriver driver) {
        try {
        	WebElement shortDescription =driver.findElement(By.id("feature-bullets")); 
        	String shortDesHtml = shortDescription.getAttribute("outerHTML");
        	if(shortDesHtml.contains("hsx-rpp-bullet-fits-message")) {
        		WebElement bulletFitsMessage = shortDescription.findElement(By.id("hsx-rpp-bullet-fits-message"));
            	shortDesHtml = shortDesHtml.replace(bulletFitsMessage.getAttribute("outerHTML"), "");
        	}
        	if(shortDesHtml.contains("replacementPartsFitmentBullet")) {
        		WebElement replacementPartsFitmentBullet = shortDescription.findElement(By.id("replacementPartsFitmentBullet"));
            	shortDesHtml = shortDesHtml.replace(replacementPartsFitmentBullet.getAttribute("outerHTML"), "");
        	}
            return shortDesHtml;
        } catch (Exception ex) {
            
            return "";
        }
    }

    public static String getStar(WebDriver driver) {
        try {
            return  driver.findElement(By.className("a-icon-alt")).getAttribute("innerHTML");
        } catch (Exception ex) {
            
            return "";
        }
    }

    public static String getTitle(WebDriver driver) {
        try {
            return driver.findElement(By.id("title")).getText();
        } catch (Exception ex) {
            
            return "";
        }
    }

    //todo: check where to get this field
    public static String getWeight(WebDriver driver) {
    	String weight = "";
        try{
        	weight = driver.findElement(By.xpath("//table[@id='productDetails_detailBullets_sections1']/tbody/tr[2]/td[1]")).getText();
        	if (weight.contains("ounces") || weight.contains("pounds")) {
        		return weight;
        	}
        	return "";
        } catch (Exception ex) {
        	try {
        		String infor = driver.findElement(By.xpath("//div[@id='detailBullets_feature_div']/ul/li[1]/span/span[2]")).getText();
        		if (infor.contains(";")) {
        			return infor.split(";")[1];
        		}
                if (infor.contains(",")) {
                	return infor.split(",")[1];
                }
                return "";
        	} catch (Exception e) {
        		return "";
        	}
        }
    }

    //todo: check where to get this field
    public static String getWidth(WebDriver driver) {
        String width = "";
        String[] dimensions = getDimension(driver);
        if (dimensions.length > 0) {
            width = getDimension(driver)[1] + "inches";
        }
        return width;
    }

    public static String getStatus(WebDriver driver) {
        String status = "";
        try {
            status = driver.findElement(By.id("availability")).findElement(By.tagName("span")).getText();
        } catch (Exception ex) {
        	status = "";
        }
        return status;
    }

    private static String[] getDimension(WebDriver driver) {
        String[] dimensions = {};
        String fullDimention = "";
        try {
            fullDimention = driver.findElement(By.xpath("//table[@id='productDetails_detailBullets_sections1']/tbody/tr[1]/td[1]")).getText();
            fullDimention = fullDimention.replace("inches", "");
            if (fullDimention.split("x").length > 1) {
                dimensions = fullDimention.split("x");
            }
        } catch (Exception ex) {
        	try {
        		fullDimention = driver.findElement(By.xpath("//div[@id='detailBullets_feature_div']/ul/li[1]/span/span[2]")).getText();
        		String[] arr = {};
        		if (fullDimention.contains(";")) {
        			arr = fullDimention.split(";");
        		}
        		if (fullDimention.contains(",")) {
        			arr = fullDimention.split(",");
        		}
            	fullDimention = arr[0].replace("inches", "");
            	if (fullDimention.split("x").length > 1) {
                    dimensions = fullDimention.split("x");
                }
        	} catch (Exception e) {
        		
        	}    	
        }
       return dimensions;
    }
}
