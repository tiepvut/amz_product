package org.amz.crawling.util;

import java.util.Random;

public class ProxyUtils {
    private static final String[] proxies = {
            "108.62.187.109",
            "108.62.187.102",
            "108.62.187.99",
            "108.62.187.100",
            "108.62.187.105",
            "108.62.187.98",
            "108.62.187.93",
            "23.81.209.131",
            "23.81.209.71",
            "23.81.209.67",
            "23.81.48.253",
            "23.81.48.238",
            "23.81.48.254"
    };
    public static String getRandomProxy() {
        Random random=new Random();
        int randomIndex=random.nextInt(proxies.length);
        return proxies[randomIndex];
    }
}
